// const showdown = require("showdown");
// converter = new showdown.Converter();
const jsonDataPath = "./public/data/data-cards.json";
const directoryPath = "./public/data/recherches";

const cardsRoutes = (app, fsp) => {
  const createSlug = (value) => {
    return value
      .replace(/[^a-z0-9_]+/gi, "-")
      .replace(/^-|-$/g, "")
      .toLowerCase();
  };

  const main = async (res, req, type) => {
    if (type === "get") {
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = JSON.parse(data);
      await res.send(data);
    }
    if (type === "create") {
      // create new card in json
      const newSlug = await createSlug(req.body.title);
      // const newSlug = req.body.title;
      await fsp.writeFile(`${directoryPath}/${newSlug}.md`, req.body.content);
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = JSON.parse(data);
      let arrayData = Object.keys(data.cards).map((key) => data.cards[key]);
      let arrWithoutCopy = arrayData.filter((obj) => obj.slug === newSlug);
      if (arrWithoutCopy[0]) {
        await res
          .status(400)
          .send(
            "ERREUR Il y'a déja une carte avec ce titre, donc c'est pas possible"
          );
      } else {
        let newCard = {
          id: `card${arrayData.length + 1}`,
          title: req.body.title,
          content: req.body.content,
          tags: req.body.tags,
          slug: newSlug,
        };
        data.cards[newCard.id] = newCard;
        let json = await JSON.stringify(data);
        await fsp.writeFile(jsonDataPath, json);
        await res.send(newCard);
      }
    }

    if (type === "put") {
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = JSON.parse(data);
      let cardToUpdate = data.cards[req.params["slug"]];

      let newCard = {
        id: cardToUpdate.id,
        title: req.body.title,
        content: req.body.content,
        tags: req.body.tags,
        slug:
          req.body.title === cardToUpdate.title
            ? cardToUpdate.slug
            : createSlug(req.body.title),
      };

      let arrayData = Object.keys(data.cards).map((key) => data.cards[key]);
      let errorSameSlug = arrayData.filter(
        (obj) => obj !== cardToUpdate && obj.slug === newCard.slug
      );
      if (errorSameSlug[0]) {
        await res
          .status(400)
          .send(
            "ERREUR Il y'a déja une carte avec ce titre, donc c'est pas possible"
          );
      } else {
        if (req.body.title === cardToUpdate.title) {
          await fsp.writeFile(
            `${directoryPath}/${cardToUpdate.slug}.md`,
            req.body.content
          );
        } else {
          await fsp.unlink(`${directoryPath}/${cardToUpdate.slug}.md`);
          const newSlug = await createSlug(req.body.title);
          await fsp.writeFile(
            `${directoryPath}/${newSlug}.md`,
            req.body.content
          );
        }
        data.cards[cardToUpdate.id] = newCard;
        let json = await JSON.stringify(data);
        await fsp.writeFile(jsonDataPath, json);
        await res.send(newCard);
      }
    }

    if (type === "delete") {
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = JSON.parse(data);
      if (data.cards[req.params["slug"]]) {
        fsp.unlink(
          `${directoryPath}/${data.cards[req.params["slug"]].slug}.md`
        );
      }
      delete data.cards[req.params["slug"]];
      let json = await JSON.stringify(data);
      await fsp.writeFile(jsonDataPath, json);
      await res.send(data);
    }
  };

  app.get("/cards", (req, res) => {
    main(res, req, "get").catch((error) => console.error(error));
  });

  app.post("/cards", (req, res) => {
    // if (validateCard(req) !== "") {
    //   console.log("error", validateCard(req));
    //   res.status(400).send(validateCard(req));
    // } else {
    main(res, req, "create").catch((error) => console.error(error));
    // }
  });

  app.put("/cards/:slug", (req, res) => {
    main(res, req, "put").catch((error) => console.error(error));
  });

  app.delete("/cards/:slug", (req, res) => {
    main(res, req, "delete").catch((error) => console.error(error));
  });

  const validateCard = (req) => {
    if (
      /^[\],:{}\s]*$/.test(
        req.body.content
          .replace(/\\["\\\/bfnrtu]/g, "@")
          .replace(
            /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
            "]"
          )
          .replace(/(?:^|:|,)(?:\s*\[)+/g, "")
      )
    ) {
      return "";
    } else {
      return "Json is not okey";
    }
    if (!req.body.title) return "Title is required.";
    if (!req.body.content) return "content is required.";
    // if (!course.tag) return "Category is required.";
    return "";
  };
};

module.exports = cardsRoutes;
