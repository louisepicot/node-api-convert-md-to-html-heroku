// const showdown = require("showdown");
// converter = new showdown.Converter();
const jsonDataPath = "./public/data/data-cards.json";
const directoryPath = "./public/data/recherches";

const createSlug = (value) => {
  return value
    .replace(/[^a-z0-9_]+/gi, "-")
    .replace(/^-|-$/g, "")
    .toLowerCase();
};

const cardsRoutes = (app, fsp) => {
  const createObjCardAndConvertMdToHtml = async (data, filename, id) => {
    // content = converter.makeHtml(data);
    const title = filename.slice(0, -3);
    const slug = filename.slice(0, -3);
    const tags = ["ressources", "brouillon", "fiche lecture"];
    let card = {
      id,
      title,
      content: data,
      tags,
      slug,
    };
    return card;
  };

  const writeJsonDataFile = async (dataConvert) => {
    let json = await JSON.stringify(dataConvert);
    await fsp.writeFile(jsonDataPath, json);
  };

  const readFiles = async (files) => {
    const Promises = await files.map(async (filename, id) => {
      const fileData = await fsp.readFile(
        `./public/data/recherches/${filename}`,
        "utf8"
      );
      const fileDataConvert = await createObjCardAndConvertMdToHtml(
        fileData,
        filename,
        id
      );
      return fileDataConvert;
    });
    const convertData = await Promise.all(Promises);
    return convertData;
  };

  const createSlug = (value) => {
    return value
      .replace(/[^a-z0-9_]+/gi, "-")
      .replace(/^-|-$/g, "")
      .toLowerCase();
  };

  const main = async (res, req, type) => {
    if (type === "create") {
      const newSlug = await createSlug(req.body.title);
      await fsp.writeFile(`${directoryPath}/${newSlug}.md`, req.body.content);
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = JSON.parse(data);
      let card = {
        id: data.length,
        title: req.body.title,
        content: req.body.content,
        tags: req.body.tags,
        slug: newSlug,
      };
      data.push(card);
      // let json = await JSON.stringify(data) ?
      await fsp.writeFile(jsonDataPath, data);
      await res.send(card);
    }

    if (type === "put") {
      if (req.params["slug"] === req.body.title) {
        await fsp.writeFile(
          `${directoryPath}/${req.params["slug"]}.md`,
          req.body.content
        );
      } else {
        await fsp.unlink(`${directoryPath}/${req.params["slug"]}.md`);
        const newSlug = await createSlug(req.body.title);
        await fsp.writeFile(`${directoryPath}/${newSlug}.md`, req.body.content);
      }
    }
    if (type === "delete") {
      await fsp.unlink(`${directoryPath}/${req.params["slug"]}.md`);
    }
    const files = await fsp.readdir(directoryPath);
    const dataConvert = await readFiles(files);
    await writeJsonDataFile(dataConvert);
    let data = await fsp.readFile(jsonDataPath, "utf8");
    data = JSON.parse(data);

    if (type === "put") {
      let modifiedCard = data.filter(
        (card) => card.slug === req.params["slug"]
      );
      modifiedCard[0] ? await res.send(modifiedCard) : await res.send(data);
    } else {
      await res.send(data);
    }
  };

  app.get("/cards", (req, res) => {
    main(res, req, "get").catch((error) => console.error(error));
  });

  app.post("/cards", (req, res) => {
    main(res, req, "create").catch((error) => console.error(error));
  });

  app.put("/cards/:slug", (req, res) => {
    main(res, req, "put").catch((error) => console.error(error));
  });

  app.delete("/cards/:slug", (req, res) => {
    main(res, req, "delete").catch((error) => console.error(error));
  });
};

module.exports = cardsRoutes;
