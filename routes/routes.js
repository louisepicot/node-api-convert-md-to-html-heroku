const cardsRoutes = require("./cards");
const chaptersRoutes = require("./chapters");
const tagsRoutes = require("./tags");

const appRouter = (app, fsp) => {
  app.get("/", (req, res) => {
    res.send("welcom to Louise Picot api server (flatfiles)");
  });

  // app.post("/login", function (req, res) {
  //   res.send("Authenticated");
  // });

  // app.post("/convert", function (req, res, next) {
  //   console.log(req.body);
  //   if (typeof req.body.content == "undefined" || req.body.content == null) {
  //     res.json(["error", "No data found"]);
  //   } else {
  //     text = req.body.content;
  //     html = converter.makeHtml(text);
  //     res.json(["markdown", html]);
  //   }
  // });

  cardsRoutes(app, fsp);
  chaptersRoutes(app, fsp);
  tagsRoutes(app, fsp);
};

module.exports = appRouter;
