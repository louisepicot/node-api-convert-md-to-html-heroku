const jsonDataPath = "./public/data/data-cards.json";

const chaptersRoutes = (app, fsp) => {
  const createSlug = (value) => {
    return value
      .replace(/[^a-z0-9_]+/gi, "-")
      .replace(/^-|-$/g, "")
      .toLowerCase();
  };

  const main = async (res, req, type) => {
    if (type === "get") {
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = JSON.parse(data);
      await res.send(data);
    }
    if (type === "create") {
      const newSlug = await createSlug(req.body.title);
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = JSON.parse(data);
      let chapterArr = Object.keys(data.chapters).map(
        (key) => data.chapters[key]
      );
      let arrWithoutCopy = chapterArr.filter(
        (chapter) => chapter.slug === newSlug
      );
      if (arrWithoutCopy[0]) {
        await res
          .status(400)
          .send(
            "ERREUR Il y'a déja un chapiter avec ce titre, c'est pas possible"
          );
      } else {
        let newChapter = {
          id: `chapter${chapterArr.length + 1}`,
          title: req.body.title,
          cardsRef: req.body.cardsRef,
          slug: newSlug,
        };
        data.chapters[newChapter.id] = newChapter;
        let json = await JSON.stringify(data);
        await fsp.writeFile(jsonDataPath, json);
        await res.send(newChapter);
      }
    }

    if (type === "put") {
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = await JSON.parse(data);
      let chapterToUpdate = await data.chapters[req.params["slug"]];
      let newChapter = {
        id: chapterToUpdate.id,
        title: req.body.title,
        cardsRef: req.body.cardsRef,
        slug:
          req.body.title === chapterToUpdate.title
            ? chapterToUpdate.slug
            : createSlug(req.body.title),
      };
      let arrayData = Object.keys(data.chapters).map(
        (key) => data.chapters[key]
      );
      let errorSameSlug = arrayData.filter(
        (obj) => obj !== chapterToUpdate && obj.slug === newChapter.slug
      );
      if (errorSameSlug[0]) {
        await res
          .status(400)
          .send(
            "ERREUR Il y'a déja un chapitre avec ce titre, donc c'est pas possible"
          );
      } else {
        data.chapters[chapterToUpdate.id] = newChapter;
        let json = JSON.stringify(data);
        await fsp.writeFile(jsonDataPath, json);
        await res.send(newChapter);
      }
    }

    if (type === "delete") {
      let data = await fsp.readFile(jsonDataPath, "utf8");
      data = JSON.parse(data);
      delete data.chapters[req.params["slug"]];
      let json = JSON.stringify(data);
      await fsp.writeFile(jsonDataPath, json);
      await res.send(data);
    }
  };

  app.get("/chapters", (req, res) => {
    main(res, req, "get").catch((error) => console.error(error));
  });

  app.post("/chapters", (req, res) => {
    main(res, req, "create").catch((error) => console.error(error));
  });

  app.put("/chapters/:slug", (req, res) => {
    main(res, req, "put").catch((error) => console.error(error));
  });

  app.delete("/chapters/:slug", (req, res) => {
    main(res, req, "delete").catch((error) => console.error(error));
  });
};

module.exports = chaptersRoutes;
