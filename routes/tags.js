const tagsRoutes = (app, fsp) => {
  const dataPath = `./public/data/data-cards.json`;
  app.get("/tags", (req, res) => {
    const main = async () => {
      const data = await fsp.readFile(dataPath);
      await res.send(JSON.parse(data));
    };

    main().catch((error) => console.error(error));
  });
};

module.exports = tagsRoutes;
