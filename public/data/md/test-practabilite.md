# TEST PRACTABILITE 

## obj pour moi,
  entrainement ecriture, creuser à fond et connaitre par coeur tt les sujets, valider mon diplome.
## formuler une phrase brève et explicite le sujet du mémoire:
  Comment les technologies (ajax) opèrent sur nos expériences du temps dans les interfaces web en 2020.
  <!-- ont shaper, modifier, influeé, transcandée, révolutionneé -->

## objet d'étude

### 1 - Ampleur de l'objet d'étude
population concerné:
<!-- designer graphique ? gens intérrésés par nouvelles technologies. -->
<!-- Dev s'intérésse au design des programes, question de diversité? coté + artistique,  -->

<!-- Peggy, lionel, pp, harisson, Renaud?, ludi, antoine gelgon, Louis*2, raphael bastide, jad, pierre huygebaert, constant, luuse, 
clemence?, quentin, anthony de masure, la head.. pzi.. annick lantenois, kevin, olivier, toune?, george, adele, geneve mec, maylis, Louise Drhule, ensad , chatonski? ux designer?.  -->
<!-- en gros les gens qui s'intérréssent au design , nouvelle techno et politique? -->
-- ++ intello/ designer graphique, geek et programmeur. 

### 2 - Situation de l'objet d'étude dans le champ de connaissance actuel.
recherches: 
- [article les-temps-et-temporalites-du-web /15m](https://www.zdnet.fr/blogs/social-media-club/les-temps-et-temporalites-du-web-39831404.htm) + 
- [eduscol def synchrone et async /1m](https://eduscol.education.fr/numerique/dossier/archives/eformation/ notion-de-temps) +
- [Robert Hassan  article temps et politique 20m](https://www.e-ir.info/2016/08/15/analogue-time-people-and-the-digital-eclipsing-of-modern-political-time/) +?
- [How Technology Changes Our Perception of Time (stress) 10m](https://thriveglobal.com/stories/how-technology-changes-our-perception-of-time/) =
- [Science Says That Technology Is Speeding Up Our Brains’ Perception of Time 10m](https://www.sciencealert.com/research-suggests-that-technology-is-speeding-up-our-perception-of-time#) ++
- [podcast simondon Du mode d’existence d’un penseur technique ](https://www.franceculture.fr/emissions/les-nouveaux-chemins-de-la-connaissance/gilbert-simondon-14-du-mode-d-existence-d-un) ++

Sujet d'actualité,
la question sur les nouvelles temporalités qui résultent de l'apparition des nouveaux médias bien qu'encore récente est largement traitée.
En revanche ce qui est moins investigué c'est plus spécifiquement les technologies d'ajax et js pour frontend.
L'enjeux est de comprendre les rouages, souvent dans l'obscurité, montrer en quoi ça a complétement changé nos rapports aux machines ect, comment ça a modifié nos éxpériences du temps et c quoi l'avenir de ces trucs.

- [ ] Faire une sélection des lectures, auteurs, sources..
- [ ] continuer recherches sur ressources qui cible plus ajax et nouvelles techno de fetch.. (rechercher dans médiarchie yves citton..)
- [ ] aaron swartz, texte  par dev sur temps de chargement d'une page
- [ ] navigateur web dev histoire ...

### 3 - Signification sociale de l'objet
quels sont les enjeux socio politique
Prise de risque? on perd notre temps --> politique question de l'attention,, 
<!-- actualité brulante en ce moment de confinement --> je le vis en ce moment -->
<!-- enjeux pour le designer graphique de reprendre les outils , penser autrement les technos qu'il utilise tt les jours.
arreter avec adobe , l'outil du designer graphique et d'iterface, controverse? -->
<!-- sujet encore peu creuser dans le monde, 
 adobe et outil fermé du designer graphique.  -->

### 4 - difficultés de l'objet
difficulté --> expliquer la techno ajax et js... comprendre navigateur fonctionnement... faire shéma uml?
techniques et interviews comliqué techniquement comment arriver à vulgariser, --> meilleur compréhension du js , de l'actualité dessus, tester les outils?...
<!-- neuroscience et discours de théoriciens compliqué--> beaucoup de lecture, prise de note... -->
neuroscience ..
<!--  recherche sur design des interfaces et évlution, parler de typo de com?  -->
lire mémoire d'autre étudiants...
<!-- organisation du mémoire pour taff collectif -->


## LE chercheur
<!-- 
### formation et capacité du chercheur
peinture et travail pictural, création d'expo, design textil.. com visuelle? typographie interet mais peu de connaissance
typographie sur le web
question de design et politique, 
programmeuse frontend 
javascript prof 
pas trés à l'aise à l'écrit
du temps pour la lecture
culture g moyenne
grande capacité de taff
ractonter des histoires, 
humour, 
poésie?
connaissance typo moyenne et com pas trop mal?
pas bcp de ref en design graphique mais en construction outils.. / theorie des médias et actualités..
connaissance du monde des strartups, problème de diversité dans progra et hyf


contact, opportunité, vivre des choses ,, -->


<!-- ### interete personel du chercheur
- me spécialiser en js , actu tech, meilleure prof de front ect.;..
- intéret pour typographie et design graphique. avoir de bonne base et culture
- ux création d'interface 
- culture numérique - txitter chercher bonne actu, prendre lestrucs marrant 
- la question de notre éxprèience du temps aujourd'hui 

### situation sociale du chercheur
- réputation, pas bonne en design 
- avoir une réputation de geek? en tt cas bonne dev, organiser, maline humble,
- je suis programmeuse et j'apprend le design graphique, je m'intérrésse au question sur les outils pc je suis une fille dans la progra c relou, 
- je suis plasticienne aussi hisotire du dessin de la peinture gashi?
- art numérique,,  -->
  
### ressources materiel
<!-- - achat de livre
- interview, 
- orga mémoire site,  -->
<!-- - doucmentation et prof --> demander vite au prof et être en contact avec eux+++/ organiser présentation, discution,,, jusqu'à fin mai seulement donc plus bcp de temps,  -->
<!-- > communiquer en ligne pendant crise pour pouvoir garder contact après. -->



## construire l'outil mémoire

- formuler une phrase brève et explicite le sujet du mémoire:
  Comment les technologies (ajax) ces technologies opèrent sur nos expériences du temps dans les interfaces web en 2020.
  ont shaper, modifier, influeé, transcandée, révolutionneé

- le design process commence dés le choix des technos et écriture du code source.
- penser aux conditions de diffusion et économie de l'outil
- adresse et penser conditions de réceptions
- design de l'interface, esthétique spécifique qui découle de l'architecture et choix techno pour construction de l'outil,
Un design personnalisable et unique pour chaque utilisation et experience de lecture (une ballade dans un paysage). C'est une rencontre avec une interface et les choix du concepteur sont pas anodins.
- Déconstruire clichés ux et habitudes, reflexes de navigation de l'utilisateur permettre une conscientisation de ses réflexes de navigation pour le désorienté et accéder à une nouvelle expèrience. 
- outil open source et informer, donner les clés pour la compréhension aide pour prise en main, construction et modification de l'outil (pas boite noir) pour le rendre accessible au plus grand nombre.
- un outil pour l'écriture de monstration et construction des textes avec oop?



<!--  construction -> comment c'est fabriqué,
- esthétique
- navigation
- outils utilisé
- structure du mémoire, organisation 
- structure des textes inspiré par oop
- déconstruire les réflexes de navigation et code ux de startup (singer our permettre un conscientisation)
- proposer avec des interfaces expèrience singulière de lecture...
- philosophie du bricolage et poésie du

construction du site
- une navigation d'abord user friendly, copier les codes d'ux pour déclencher des réflexes de navigation puis déstabiliser l'utilisateur, le désorienter pour provoquer une conscientisation de ses réflexes de navigation. Lui faire réaliser qu'il est orienter et lui permettre de faire un pas de coté envisager la navigation et nouveau rapport à l'interface.

- une esthétique de l'interface qui a qq chose d'organique, de tangible grumeleux une plasticité, donner une matèrialité casser frontières entre travail plastique et web

- organiser mes idées, corpus de textes, expèrience, synthétiser, épurer évider distiller.. récupérer le substrat.
Pouvoir avoir tout au meme endroit, permettre une vision d'ensemble afin de créer des chemins de penser et des associations innatendues.

- des outils expèrience uniques et pas ultra performant, avertir utilisateur, des aventures et une rencontre avec une interface, la diffusion et l'utilisation par d'autres...


- des programmes qui font pas bien ce qu'on leur demande
des courts programmes indiscplinés, surprenants et entohusiastes qui produisent des formes singulières qui feront office d'illustration pour venir épauler chaque chapitre.
Créer à un moment pour répondre à une urgence : "comprendre un concept, exprimmer une émotion précise..


- comment organisation du code et écriture programmatique influence écriture du mémoire
- création d'un outil pour le mémoire:
    - Je souhaite m’inscrire dans une démarche qui tend à effacer la séparation classique entres les phases d’élaboration du design et de l’écriture du code source, en partant de l’idée que le Design Process est inhérent aux choix des technologies, à la construction de l’arborescence de l’application, et à l’écriture du code source. 

- création d'une palette d'outils graphics et pour générer des formes spécifique et en relation avec l'architecture et choix techno des programmes qui les construisent
- choix du language, du framwork front...
- environnement de travail... 
- travail en collaboration ave mon navigateur, des logiciel gratuit pen-source ou pas... 
- collaboration et versionning avec git et choix de la plateform gitlab
- choix hebergeur nom de domaine...
- actualité 
  la culture des programmes numériques et informatiques, les interfaces web et les infrastructures des réseaux.
- Comment la rupture numérique à fait évoluer la pratique et les processus de création des designers et artistes.
- Je me questionne sur les possibilités qu’offrent aujourd’hui le développement d’interface en 3d sur le web. Ces nouvelles expériences permettent de faire l’expérience de l’embodiment , de se positionner dans un espace, choisir un point de vue , s’orienter..  -->