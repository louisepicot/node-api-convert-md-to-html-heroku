# engineering care


Dans son livre "Qu'est ce qu'un dispositif?" de Georgio Agamben décrit les dipositifs comme des objets qui à un moment donné on eu pour fonction majeure de répondre à une urgence. Je me suis intérréssée à la crise des vocations des aides-soignants en France et plus précisement dans les maisons de retraites. Malgrès leurs prix souvent très élévés , de nombreux scandales ont éclatés ces dernières années sur les problèmes de maltraitance, vol, insalubrité , et conditions de soin catastrophiques dans les ehpads. Pour palier à ce problème ,des assistants personnels intelligents, des robots thérapeutiques et des compagnons humains sont en développements. En tant que machines destinées à prodiguer des soins, elles informent, rappellent, tiennent compagnie, servent d'intermédiaires et fournissent une rétroaction pour permêttre aux personnes âgées de vivre une vie plus sûre et autonome. De nombreux business se devellopent en ce moment un nouveau marché et nouvelle économies. Le remplacement du personnel par ses soignants artificiels qui sont censés entreprendre une forme de travail qui est affective, invisibilisée et sous-estimée posent de nombreuses questions sur les enjeux sociopolitique et le devenir des intéractions sociales dans ce type d'établissement. Pour en savoir plus sur cette actualité j'ai rencontré un game designer et entrepreneur qui travail en collaboration avec une maison de retraite sur des projets de jeux vidéos. Les objectifs premiers de ses jeux sont de divertir, créer du liens entre les occupants et potentiellement aider à prévenir la perte de mémoire et autres formes de sénilité. L'entrepreneur m'a avoué que Les jeux sont aussi conçu pour provoquer des secretions de dopamine afin de pousser l'utilisateur à persister le plus longtemps possible et à les rendre plus attractifs. Un bois des matèriaux rudimentaire.
Les technologies ont de moins en moins pour but de nous lier aux autres mais plutot de nous lier à nous même. La gamification des intéractions est utilisé pour nous liéer d'avantages au produit.


## methodo

1. rencontre sasha interview
2. écrire texte sur ambition du robot cynisme de startupeur + une image humour style d'écriture particulier 
    - en Anglais language de startupeur
    - personna
    - psycho du robot, poésie du language
    - comment le construire?
3. site sur les startup qui invente des robots pour vieux
4. recheches sur l'état d'avancement sur quantified self
5. article + podcast sur pb dans maison de retraite
6. début de création de l'interface (lettre)
7. inventer le dispositif?

## bibliographie

- Médiarchie de Yves Citton
- "Qu'est ce qu'un dispositif?" de Georgio Agamben
- Xavier de la porte /plusieurs chroniques
- texte pour appel à candidature résidence web
- la gamification