INTRO

Who can wait quietly while the mud settles? Who can remain still until the moment of action? Laozi, Tao Te Ching

Aaron swartz dans un de ses articles nous dit que les nouvelles technologies deviennent rapidement si omniprésentes dans nos quotidiens qu'il est parfois difficile de se souvenir de comment les choses étaient avant.

La puissance du cerveau humain c'est sa capacité à créer du vide. C'est ce qui permet d'apprendre, de comprendre et d'imaginer. Dans son livre Médiarchie Yves citton explique que dans notre génération les nouveaux médiums nous accablent en permanence de stimulus, et en déclanchant chez nous des besoins de réponses instantanées, ils nous empechent de créer le vide nécéssaire pour comprendre, annalyser et construire une réfléxion sur les informations reçus.

En règle générale on remarque que nos appareils de médiation tendent à rester invisibles ou transparents tant qu’ils fonctionnent selon ce que nous attendons d’eux.

En éléminant les frictions sur les sites web du au temps de rafraichisement des pages à chaque nouvelle request au serveur, la technologie Ajax (Asynchronous javascript and XML) à grandement participé à créer ce rythme arrassant de boucles retroactives.

Pour mon mémoire je souhaite m'intérrésser à cette technologie qui a révolutionné le web tel que nous le connaissons aujourd'hui et continue de modeler l'avenir de nos navigation. Elle est aussi en ce moment au coeur de l'actualité du dévellopement web car c'est sur elle que sont basés les frameworks frontend javascript de plus en plus largement utilisés.

Même si cette évolution peut s'embler anodine ou infime dans l'histoire du web, elle est aujourd'hui omniprésente dans nos quotidiens et je pense qu'il est intérréssant pour le designer d'interface, de web, et théoriciens de la novuelle communication de comprendre le fonctionnement de ces technologies, leurs histoire et comment elles ont pu sans crier gard modifier nos preception du temps et temporalité devant nos écrans.

Pour mieux comprendre comment ces technologies opèrent sur nos expériences du temps dans les interfaces web, je souhaite réaliser une série de courts programmes pour dans le but de mieux comprendre et éclairé sur les différents concepts parcouru dans mon mémoire.

Les résultats de ces programmes seraient des formes et éxpériences d'interface singulières qui viendraient questionner nos rapport du temps devant nos écrans. Aussi les formes générés pourraient elle même éclairer sur les étapes de la conception du programme, sur son architecture, et permettre visuellement de comprendre ses rouages et mécanismes interne.

Pour construire ce mémoire je construit un outil qui me permet de penser des formes d'écriture… et qui lui donnera sa forme finale.

Avec l'actualité du coronavirus, la quarantaine, l'invisibilté de ces mécanisme qui passent dans nos vie et modifient notre quotidien insidueusement. Comment ça s'immisce et qu'est ce que ça déplace?

exemple c'est facebook qui a developpé react et c'est eux aussi cambridge analitica coincidence🤔🤯? 
