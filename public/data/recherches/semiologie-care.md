# fiche lecture
# Une ethnographie de la relation d’aide : de la ruse à la fiction, ou comment concilier protection et autonomie

Les soignants doivent composer entre des aspirations contradictoires comme la protection et l'autonomie, et assumer des prises de risques.
p.3 (sur les personnes agés qui ne veulent pas quitter leur maison pour aller en maison de retraite)

Questions posés dans le rapport: Comment faire faire quelque chose à des personnes qui ne l'ont pas demandé ou n'en voient pas l'intérêt.
p.3

Vivre chez soi c'est pouvoir continuer à être soi, et c'est garder un droit de décider comment les choses doivent se faire.