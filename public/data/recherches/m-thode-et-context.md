Ce que je redoute le plus pour l'instant c'est l'assemblage du puzzle, le choix de l'organisation finale de tout les fragments. 
C'est là dedans, à mon sens, que vas se trouver l'intérêt du texte. Ce travail va de pair avec la mise en page de l'objet et les choix esthétiques de sa présentation. 
J'aimerais qu'il soit "Superficielle par profondeur" comme qui disait Nietsch. 

J'aimerais avoir une mémoire hors norme, qui me permettrait d'accéder en permanence à une visualisation de ma recherche dans son ensemble. 
Pouvoir construire une image mentale de l'architecture et faire le travail de hiérarchisation des parties comme dans The Queen's Gambit quand elle imagine un échiquier sur son plafond. 
J'aimerais pouvoir dire que mes réflexions sur la gouvernance d'internet, les révélations d'Edward Snowden, et la théorie des médias ne me laissent pas tranquille, mais ce n'est pas le cas. 
La magie finale va résider dans les jointures de la construction, et peut être aussi dans les anecdotes et détails humoristiques qui seront comme les posters plaqués dans les couloirs du bâtiment.

Bref, la difficulté ça va aussi surement être de finir. 
Jul écrit ses textes en 5 minutes mais il en écrit en mass, et dans le tas, à force d'acharnement il y'a bien quelques chefs d'oeuvre, comme les assiettes que Picasso produisait à la chaine. C'est dangereux de s'embourber dans une quantité astronomique de recherche. 
Comme en peinture, il faut savoir s’arrêter, et au bon moment, pour enfin commencer le travail fastidieux d’artisans.

Je me suis fixé une date limite pour finir ce mémoire.
Pour un morceau de musique il faut l'apprendre par partie en le pratiquant quotidiennement. Il faut ensuite tenter de le jouer en entier jusqu'à ce que ça rentre jusqu'au bouts des doigts, puis enfin chercher la musique dans les nuances. L'émotion c'est bien plus tard, c'est quand on oublie tout, qu'il n'y a plus de techniques, plus de solfège, plus de douleurs sur les cornes, il n'y a plus que l'attente du silence d'après, contaminé par les sons d'avant.

Dans le podcast Bookmakers sur Arte Radio, l'écrivain Philippe Jaenada cite une phrase de Deleuze "Un grand écrivain c'est un étranger dans sa propre langue. Il ne mélange pas notre langue à sa langue, il taille dans sa langue une langue étrangère et qui ne préexiste pas."
Ce qu'il faut comprendre c'est qu'on construit chacun une langue singulière à partir de la langue française.
Pour avoir du style, il faut chercher à ce que notre écriture soit au plus proche de ce qu'on est, qu'elle soit une transcription de notre magma intérieur, et révéler les spécificités de notre version unique de la langue.
La méthode de Philippe c'est d'écrire par paragraphe et de "rouler" de phrases en phrases à l’intérieur pour avancer en gardant le fil.
Ce que j'ai surtout retenu c'est qu'il faut bien connaître son sujet pour que la structure et le style puissent suivre naturellement.
Ce podcast a fait comme un déclic, c'est comme çi j'avais ouvert une vanne et depuis les mots coulent à flot.

J'ai relu plusieurs fois en entier toute ma recherche.
Ensuite j'ai construit un plan intermédiaire composé de 5 à 6 chapitres avec 2 ou 3 sous chapitres.
L'objectif, quand tu écris un programme, c'est de synthétiser au max le code pour trouver la version la plus neat de l’algorithme.
Pour pouvoir réorganiser facilement mon mémoire, avoir une vision d'ensemble accessible de partout, construire la mise en page, la structure, collaborer avec mon promoteur (et ma mère) sur la relecture, puis, partager facilement mon travail, j'ai décidé de construire un outil web.
C'est l'idée du "model du classeur" décrit dans le livre "Comment réussir son mémoire" par Jean-Pierre Fragnière qui m'a inspiré.
J'ai commencé par acheter un classeur blanc, des feuilles plastifiées et des intercalaires colorés pour les chapitres.
Ensuite j'ai imprimé tout mon fouilli sur des feuilles simples pour pouvoir les découper, réorganiser et les griffonner de partout.
Un intercalaire correspond à un chapitre et à l’intérieur j'y place des notes , fiches de lectures, fiches bibliographiques, images, ect...
Après être arrivé à un certain stade d'avancement ou une certaine date, il faut faire un break, tout relire, puis passer au plan définitif.
Ensuite à un moment t, il faudra stopper l'accumulation de datas pour passer à la phase de rédaction.
Dans cette partie finale j'imagine qu'il s'agit de faire de jolies phrases pour assembler le tout.
Mais il ne faut pas perdre de vue la cible, "le persona" comme disent les startupeurs.
Il faut aller à l'essentiel en restant au plus proche du plan pour ne pas partir dans tout les sens, ou faire de l’introspection.
Ensuite il faut divertir entre les lignes, vulgariser le charabia technique et infiltrer une dose de suspens...

Le but de cet outil c'est aussi de pouvoir retrouver rapidement des notes et lier des parties en utilisant un système de tags, créer des juxtapositions inopinées, et gagner du temps grâce à un outil de recherche efficace.

La contrainte c'est une béquille importante, voir nécéssaire, mais à l'erg les limites sont parfois un peu flou, donc j'ai finis par trancher.
Je me suis fixé une trentaine de pages, un mémoire papier imprimé en 8 exemplaires, en utilisant une méthode plutôt traditionnelle.

Je suis programmeuse web et si j'ai réussi à me faufiler dans cette vocation, sans avoir fait les études adéquates ni être un génie des maths, c'est surement en grande partie grâce à mon esprit de contradiction qui cherche à démentir les clichés sociétaux sur les geeks et surtout grâce à mon talent héréditaire pour la flemmardise.
Si j'ai décidé de coder l'outil c'est aussi parce-que ça m'ennuie d'utiliser adobe: je n'aime pas être dépendante d'un instrument; et je ne vais pas m'amuser à faire la mise en page à la main en collaboration avec une imprimante: 1 ça prendrait beaucoup trop de temps et 2 c'est pas écologique.

Pour le Jazz il faut arriver au niveau où tu peux te le permettre.
L'improvisation en Jazz, c'est pour les génies, ou ceux qui se sont sacrifiés pour la musique, sinon c'est imbuvable.
Serge Gainsbourg c'est les deux j'imagine. Il peut se permettre de jazzifier les plus grands compositeurs classique parce-que c'est une bête de travail additionné d'un génie punk.

Entre la phase du deuxième classeur (le plan définitif) et la rédaction il faut essayer de faire simple, finir l'épuration pour extraire le substrat.
L'essentiel c'est la définition précise de l'objet d'étude, les étapes de résolution de la problématique, puis de rester au plus près de ce que je suis pour les finitions.

Revenir sur les moments de construction de mon outil ça veut aussi dire replonger dans les souvenirs douloureux du premier confinement.
Pour être honnête, écrire ce mémoire aujourd'hui c'est comme tenter de me retirer une épine du pied sans vraie garantie de réussite.
Bien sur que c'est pour tout le monde pareil, mais j'ai passé le mois de mars 2020 devant ma fenêtre, à m'angoisser à l'idée qu'un proche meure seul dans un hôpital pendant que je suis à des centaines de kilomètres, en train de manger des deliveroo avec comme point de mire la raie du cul de mon voisin d'en face qui clope sur son balcon.
Dans le début de La Nausée Sartre parle de sa solitude: "Quand on vit seul, on sait même plus ce que c'est de raconter: le vraisemblable disparaît en même temps que les amis. Les événements aussi on les laisse couler; on voit surgir brusquement des gens qui parlent et qui s'en vont, on plonge dans des histoires sans queue ni tête : on ferait un exécrable témoin."
C'est un peu comme quand tu mates toute la série des Harry Potter d'une traite, puis que tu sors au supermarché au bout de 3 jours avec le teint pâle et un regard hagard, tu as oublié comment avoir une interaction sociale alors tu t'efforces de ne pas dire "Wingardium Leviosa" au lieu de merci à la caissière.

Sartre dit aussi qu'il y aurait un danger à fricoter trop avec la solitude, le risque ce serait de glisser de l'autre côté de la frontière, dans un abîme dangereux: "Mais je restais tout près des gens, à la surface de la solitude, bien résolu, en cas d'alerte, à me réfugier au milieu d'eux [...]".

Depuis le premier jour chez moi j'ouvrais grand les oreilles pour être au plus proche de mes voisins de palier (et pallier à mon manque de voisins).
Je ne leur avais pourtant, jusque là, pas accordé beaucoup d'importance.
Au début j'ai été surprise par le silence, le soir il n'y avait plus aucun bruit dans la rue Antoine Dansaert d'habitude très animée.
La journée j'entendais des murmures, le calme, les sonneries des appels skype, les disputes du couple d'italiens de droite et leurs conversations inquiètes avec leurs familles en Italie, les applaudissements de mes voisins à 20h, le concert de piano de l'ado à gauche, et souvent les cris hystériques de ses parents qui craquent sur les plus petits de la fraterie. 
Et le soir du troisième jour j'ai entendu un cri.

Sartre dit qu'à ce niveau de solitude on laisse passer les évènements vraisemblables mais du coup par compensation "tout ce qui ne pourrait être cru dans les cafés, on ne le manque pas".
Je ne suis pas sortie rapidement pour le cri parce-que j'avais l'impression de nager dans une sorte de rêve apocalyptique  depuis le début de cette pandémie, ou une mauvaise série B de science fiction, et ce soir là je n'étais pas sur d'avoir envie d'un nouvel épisode de Stargate Sg1.
Finalement je suis sortie sur mon balcon et j'ai vu une femme agenouillée devant une forme enfantine qui gisait par terre, et la fenêtre du 4ème étage était ouverte.
Les flics sont arrivés dans la rue vide, ils regardaient la scène les bras ballants sans rien dire, le père a pris l'enfant dans ses bras, les pompier ne sont pas venus, ils ont arrêté de crier et je suis retourné dans mon lit.
Je pense que c'est à partir de là que je me suis plongée dans la construction de l'outil .
Pendant une semaine j'ai travaillé dessus frénétiquement, non stop jour et nuit.

Philipe Jaenada dit qu'il roule dans le paragraphe jusqu'à trouver un certain équilibre.
Il relit le texte mais pas à voix haute.
Puis quand il est convaincu il passe au suivant et s'impose de ne plus y revenir pour éviter de le modifier indéfiniment.
Je vais utiliser cette méthode. Écrire par paragraphe, sans vraiment m'occuper de la cohérence de l'ensemble.
A la toute fin je reviendrai sur les fragments, j'en supprimerai et je réagencerai le tout pour en faire émerger le sens.