Le noir est comme un bûcher éteint, consumé, qui a cessé de brûler, immobile et insensible comme un cadavre sur qui tout glisse et que rien ne touche plus. Vassili Kandinsky – Du spirituel dans l’art

Il vaut mieux ne pas jouer le rôle du temps, il travaille mieux que nous. Vassili Kandinsky – Extrait d’une Lettre à Herwarth Walden – Novembre 1913

Le blanc sonne comme un silence, un rien avant tout commencement. Vassili Kandinsky


Wassily Kandinsky, né à Moscou en 1866, va mettre sens dessus dessous les canons de la peinture. Précurseur de l’art abstrait[1] , il va théoriser cette « découverte » et se faire connaître fin 1911, en publiant « Du spirituel dans l’art ». Cet ouvrage délivre l’art de la dictature du réel et le place dans une dimension spirituelle. En jouant sur les couleurs et les formes, l’artiste est en mesure de révéler une vérité cachée derrière une vérité d’évidence, trop souvent éloignée de la réalité.