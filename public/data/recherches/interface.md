L’interface est divisée en trois colonnes.
 Commençons par celle de gauche, la liste des chapitres. 
Pour ajouter un nouveau chapitre il suffit de cliquer sur le bouton du haut « ajouter un chapitre ». 
Tout en bas il y’a le chapitre « draft », qui ne peut pas être supprimé.  
C’est dans ce chapitre que vont venir se mettre par défaut toutes les nouvelles notes, ainsi que celles qui se trouvaient dans un chapitre fraîchement supprimé.  
En drag and droppant les titres des notes à l’intérieur des différents chapitres on peut les réagencer à notre guise, et les supprimer en cliquant sur la croix en bout de ligne. 
--Il y’a aussi un bouton « add subchater » à côté de « add chapter » (pour ajouter un sous-chapitre).--
 
 L’idée de ce design d’interface est largement inspirée de de mon éditeur de texte.
 Comme dans Visual studio code, la colonne de droite permet d’avoir une vue d’ensemble sur la structure du dossier, de hiérarchiser, modifier les titres des différents fichiers et de les ouvrir dans la colonne centrale en cliquant simplement dessus.
  Un éditeur de texte c’est un logiciel gratuit et facile à installer. 
 En soi, si vous voulez coder dans word ou n’importe quel logiciel de traitement de texte y a pas de problème, juste vous allez perdre un temps monstre. 
Le but de ces logiciels c’est de simplifier la vie des programmeurs avec de jolies couleurs et des highlights pour les erreurs dans le code, des shortcuts à gogo et des extensions open sources gratuites qui marchent du feu de Dieu pour coder comme un pro.  
Ensuite la colonne centrale, c’est la liste des notes.  Ici on peut ajouter une note, la modifier ou la supprimer.
 Une note est composée d’un titre et un contenu obligatoires et on peut si on souhaite ajouter des tags. 
Dans la bar de recherches de la navbar on peut filtrer avec une liste de tags pour afficher uniquement les notes qui nous intéressent.
  Pour le contenu des notes, on peut utiliser le langage markdown pour faire la mise en page.
 Markdown, c’est « un langage de balisage léger », inventé en partie par Aaron Swartz.
 Techniquement ce qu’il se passe, c’est qu' en sauvant la note, l’objet est directement envoyé tel quel au backend pour être stocké dans la db, puis j’utilise la librairie javascript "showdown" quand mon component est render par l'app react dans le front pour convertir le markdown en html. 
J’ai choisi le markdown parce que c’est facile à utiliser et on peut coder directement en html si on le souhaite.
 Le markdown est souvent utilisé pour faire des mises en page sur des sites comme GitHub ou reddit.
 Ça se repère facilement comme esthétique.  
En bas de la liste des notes il y a un bouton « top » qui est fixe, il permet de remonter à toute vitesse en haut.
 J’ai mis un effet de transition rapide pour qu’on comprenne bien qu’on est train de scroll vers le haut quand on clique dessus.
  Ensuite la dernière colonne, celle de gauche, c’est pour les illustrations, images, site internet qui sont en lien avec les notes, chapitres ou tags.
 L’idée c’est d’avoir une sorte de gallérie qui s’organise différemment en fonction du filtrage par tags, ou par chapitre.
 Quand on clique sur un titre de chapitre dans la colonne de gauche, toutes les images se rehaussent à droite pour que celles qui sont en lien viennent se glisser en premier.  
Après, la dernière features importante c’est le « click to print ». En cliquant dessus on arrive sur page qui permet de visualiser le rendu avant impression grâce à la librairie paged.js.
