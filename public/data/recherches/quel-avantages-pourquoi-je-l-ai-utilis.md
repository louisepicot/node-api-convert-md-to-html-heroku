- aide pour oganisation et méthodologie
- utilisation des boards (done, to do ,...) pour visualiser facilement les prochaines étapes à réaliser, et diviser mon travail en taches avec différents niveaux d''importances.
- Au cas ou je m'embale et je crée une features complètement inutile ou que je modifie n'importe quoi par inadvertances je peux revenir dans le temps à une version d'avancement antérieur du projet.
- je peux si je le souhaite construire deux versions sur deux branches différentes?
- je peux sur gitlab consulter la list de tout mes commits et de toutes les issues que j'ai fermés. Un historique et une trace de toutes les étapes de mon travail.
Me premet aussi de gerer mon temps en faisant des estimations de temps pour chaques taches à accomplir.
- 3 categories : BUG (urgent), (STYLE), (TO DO)