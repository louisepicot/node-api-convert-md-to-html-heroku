
Quand on tape ajax sur google on tombe sur le score du dernier match d’Ajax Amsterdam (un club de football néerlandais) puis sur un article wikipedia des deux héros Ajax de la guerre de Troie dans la mythologie grec.
Pour la page d’Ajax le Petit, au chant II de l’Iliade, dans le Catalogue des vaisseaux, on lit1 : 
« Les Locriens obéissent au fils d'Oïlée, Ajax le rapide. Il n'a pas la taille du fils de Télamon ; il est moins grand que lui, beaucoup moins grand même. Mais en dépit de sa petite taille et de sa cuirasse de lin, pour lancer la javeline, il n'a pas de rival parmi les Panhellènes ou les Achéens. » 

Dans son article sur Ajax, Aaron Swartz commence par dire "les nouvelles technologies deviennent rapidement si omniprésentes dans nos quotidiens qu'il est parfois difficile de se souvenir de comment les choses étaient avant. »
La technologie Ajax s’est immiscée insidieusement dans nos vies, et comme Ajax le petit elle n’a pas vraiment de rival.
Du jour au lendemain on s’est mis à être interrompu au quotidien par les bruitages des notifications instantanées sur les réseaux sociaux.
C’est grâce à cette technologie que les design d’interfaces inspirés des casinos et machines à sous ont pu être développés, afin de toujours mieux phagocyter notre temps d’attention. 
L’ « infinite scroll » s’est construit de manière à rythmer sournoisement nos sécrétions de dopamine pour nous faire perdre la notion du temps en nous aspirant dans une sorte d’hypnose visuelle.
Sans faire de vague ni crier gare, en éliminant les frictions sur les sites web du au temps de rafraîchissement des pages à chaque nouvelle request au serveur, Ajax à donc grandement participé à créer un rythme arasant de boucles rétroactives, et accélérer le bombardement du flux d'information sur nos écrans.
La puissance du cerveau humain c'est sa capacité à faire du vide. 
Les interfaces web nous accablent aujourd’hui en permanence de stimulus, et en déclenchant chez nous des besoins de réponses instantanées, elles nous empêchent de créer le vide nécéssaire pour comprendre, analyser et construire une réflexion sur les informations reçus.
Dans un des prologues de l’émission « le code à changé «  par Xavier de La porte, je me souviens qu’il parle du rapport entre la mémoire et nos temporalités.
Comme on arrive plus à processer correctement la grande quantité d’information qu’on ingurgite chaque jour, on ne mémorise plus et donc fatalement on finit par ne plus voir le temps passer, parce-que la mémoire c’est l’oublie.
Pour ne pas avoir a faire d’effort, on s’en remet sereinement aux médias pour stocker nos informations, même les plus intimes. 