##Choix de l’objet d’étude

Sometimes there's a man… I won't say hero, 'cause what's a hero? But sometimes there's a man, and I'm talking about the Dude here. 
Sometimes there's a man… Well, he's the man for his time and place. He fits right in there. And that's the Dude in Los Angeles. And even if he's a lazy man, and the Dude was most certainly that, quite possibly the laziest in Los Angeles County, which would place him high in the running for laziest worldwide, but sometimes there is a man, sometimes there's a man… Well… lost my train of thought here. 
But… aw hell… I done introduced him enough. 
Sam Elliott, The Big Lebowski (1998), écrit par Ethan et Joel Coen 
￼

Pour le choix de l’objet d’étude j’ai longtemps hésité.
J’ai principalement oscillé entre la question de l’engineering care et des nouveaux robots intelligents dans les maisons de retraite, le post-colonialisme au Vietnam et le souffle nouveau dans l’art contemporain après 30 ans de guerres, puis l’évolution de nos expériences du temps sur les interfaces web depuis l’avènement de la technologie AJAX (pour asynchronous javascript and xml).
Ces trois objets d’études n’ont pratiquement rien à voir et pourtant ce fut très difficile de les départager.

Pour cette dernière option j’ai effectué un test de practabilité.
D’abord j’ai cherché à délimiter l’ampleur de l’objet d’étude, c’est à dire me demander concrètement qui pourrait être intéressé par ce sujet.
Quand je donne le cours du « product pitch » au wagon (le coding BootCamp pour qui je travail occasionnellement), j’introduit la notion de « persona ».
Le persona c’est l’utilisateur type du produit, la « target » de l’application web.
Le mieux c’est toujours d’arriver à être au plus proche de son persona. 
Pour ça il faut choisir un problème à résoudre pour quelqu’un de notre entourage, ou bien encore mieux, qui nous touche nous même directement.
Il faut pouvoir se glisser dans ses baskets, connaitre son quotidien, son environnement, et même pouvoir anticiper ses actions.
Aussi chez les startupeurs on parle du concept de « pain solving » et on dit que l’idée est « overreated » car c’est uniquement la manière dont on va apporter une solution nouvelle à un problème concret qui compte.
Etre le premier à avoir l’idée ce n’est étonnamment pas vraiment signe de succès , avant BlablaCar il y’a avait déja des sites de covoiturages et des taxis avant Uber.
Surtout, en plus d’être rassurant, avoir des concurrents ça permet d’avancer plus vite en piquant les trouvailles des autres puis briller en trouvant un moyen de sortir du lot.
Bien sûr je suis loin d’adhérer à tout les préceptes cyniques que je prêche aux requins en costumes Celio du Wagon, mais de toute façon ils sont souvent plus fascinés par mon 90d que par mes talents d’oratrice…

Construire un outil pour m’aider à écrire mon mémoire c’était pas d’emblée une idée très sexy, et c’est aussi pour ça que ça m’a plu tout de suite.
J’ai des difficultés à écrire à cause de la dyslexie et je n’avais jamais eu à construire un texte de cette ampleur, j’étais donc assez pétrifiée par la prospective de cet exercice.
Le but était de me simplifier la tâche au maximum et j’étais mon propre persona donc je pouvais facilement caractériser toute les facettes du problème.
Je voulais aussi que le design de l’outil finisse par influencer mes chemins de pensée, m’aide à expérimenter de nouvelles formes d’écriture, et en bout de course donne une singularité à la forme final de l’objet imprimé.
Malheureusement j’ai fait une erreur de débutant(e).
C’est très risqué de s’enfermer pendant des jours pour coder un outil sans avoir de point de vue ni avis extérieur. 
Ce que je répète aussi souvent aux startupeurs c’est qu’avant d’écrire la première ligne de code il faut s’assurer, en testant le produit, que des gens sont près à payer. 
Avant cette étape, même si votre famille, vos collègues, Elon Musk* et Marc Zuckerberg, vous disent que c’est l’idée du siècle vous risquez fort de vous prendre un mur.
En plus aujourd’hui c’est très simple de tester une idée, il suffit d’une « landing page » qui décrit le produit, un formulaire d’inscription et un lien PayPal.
Comme je n’ai pas pensé à l’avenir de cet outil mais uniquement à l’urgence de l’écriture du mémoire, et que je n’ai pas voulu m’intéresser à l’accueil de mon outil dans le monde réel (en dehors de ma bulle de confinée),
mon code est rapidement devenu un monstre tentaculaire indomptable avec des dizaines de fonctionnalités complexes et un ux* repoussant que j’étais la seule à pouvoir déchiffrer.
Quand on a le nez dans le guidon en programmation on s’embourbe rapidement dans des spaghettis apocalyptiques.


Ajax

Quand on tape ajax sur google on tombe sur le score du dernier match d’Ajax Amsterdam (un club de football néerlandais) puis sur un article wikipedia des deux héros Ajax de la guerre de Troie dans la mythologie grec.
Pour la page d’Ajax le Petit, au chant II de l’Iliade, dans le Catalogue des vaisseaux, on lit1 : 
« Les Locriens obéissent au fils d'Oïlée, Ajax le rapide. Il n'a pas la taille du fils de Télamon ; il est moins grand que lui, beaucoup moins grand même. Mais en dépit de sa petite taille et de sa cuirasse de lin, pour lancer la javeline, il n'a pas de rival parmi les Panhellènes ou les Achéens. » 

Dans son article sur Ajax, Aaron Swartz commence par dire "les nouvelles technologies deviennent rapidement si omniprésentes dans nos quotidiens qu'il est parfois difficile de se souvenir de comment les choses étaient avant. »
La technologie Ajax s’est immiscée insidieusement dans nos vies, et comme Ajax le petit elle n’a pas vraiment de rival.
Du jour au lendemain on s’est mis à être interrompu au quotidien par les bruitages des notifications instantanées sur les réseaux sociaux.
C’est grâce à cette technologie que les design d’interfaces inspirés des casinos et machines à sous ont pu être développés, afin de toujours mieux phagocyter notre temps d’attention. 
L’ « infinite scroll » s’est construit de manière à rythmer sournoisement nos sécrétions de dopamine pour nous faire perdre la notion du temps en nous aspirant dans une sorte d’hypnose visuelle.
Sans faire de vague ni crier gare, en éliminant les frictions sur les sites web du au temps de rafraîchissement des pages à chaque nouvelle request au serveur, Ajax à donc grandement participé à créer un rythme arasant de boucles rétroactives, et accélérer le bombardement du flux d'information sur nos écrans.
La puissance du cerveau humain c'est sa capacité à faire du vide. 
Les interfaces web nous accablent aujourd’hui en permanence de stimulus, et en déclenchant chez nous des besoins de réponses instantanées, elles nous empêchent de créer le vide nécéssaire pour comprendre, analyser et construire une réflexion sur les informations reçus.
Dans un des prologues de l’émission « le code à changé «  par Xavier de La porte, je me souviens qu’il parle du rapport entre la mémoire et nos temporalités.
Comme on arrive plus à processer correctement la grande quantité d’information qu’on ingurgite chaque jour, on ne mémorise plus et donc fatalement on finit par ne plus voir le temps passer, parce-que la mémoire c’est l’oublie.
Pour ne pas avoir a faire d’effort, on s’en remet sereinement aux médias pour stocker nos informations. 


Le quantified self
Marie interview
—> marie supprime les adds sur instagram,
- double identité/ l’identité numérique 
- poser des questions?



Connais toi toi mm, mais surtout laisse ton téléphone te connaitre mieux que toi !!!

Le quantifier self 
comment laisser 



La sensibilité au temps s'améliore pendant l'enfance du fait du développement des capacités d'attention et de mémoire qui dépendent de la lente maturation du cortex préfrontal.
Afin d’être jugé correctement le temps demande non seulement de lui prêter attention, mais aussi de conserver en mémoire le flux de l'information temporelle et de maintenir une attention soutenue.

“nos appareils de médiation tendent à rester invisibles ou transparents tant qu’ils fonctionnent selon ce que nous attendons d’eux. Ils ne s’imposent pleinement à notre attention – sur le mode de la surprise traumatique – que lorsqu’une panne fait apparaître la dépendance impuissante que nous avons développée à leur égard”

Excerpt From: Yves Citton. “Médiarchie.” iBooks. 


Le quantified self (mesure de soi en français1) ou personal analytics est un mouvement qui regroupe les outils, 
les principes et les méthodes permettant à chacun de mesurer ses données personnelles, de les analyser et de les partager2. 
Les outils du quantified self peuvent être des objets connectés, des applications mobiles ou des applications Web. 


Who can wait quietly while the mud settles? Who can remain still until the moment of action? Laozi, Tao Te Ching

 à cette technologie qui a révolutionné le web tel que nous le connaissons aujourd'hui et continue à être d'actualité et de modeler l'avenir de nos pages web car c'est sur elle que les framwork frontend javascript très en vogue en ce moment sont basés. 




xavier de la porte prologue "le code à changé" et Annie Ernaud "les années"


-isabelle étrangers sénile, la notion du temps et la mémoire




Cette avancée technique peut sembler anodine ou infime dans l’histoire web, pourtant elle a radicalement fait évoluer la façon dont les interfaces web opèrent sur notre expérience du temps.

En règle générale on remarque que nos appareils de médiation tendent à rester invisibles ou transparents tant qu’ils fonctionnent selon ce que nous attendons d’eux.





Les nouveaux médias tendent à rester invisibles tant qu’il font se que nous attendons d’eux 











La technologie Ajax est une avancée technique qui peut sembler anodine ou infime dans l'histoire du web. 
Elle a pourtant radicalement transformé nos interactions avec les interfaces web et a bouleversé nos temporalités devant les écrans.
Avec les bruitages des notifications instantanées , elle est aujourd'hui omniprésente dans nos quotidiens et elle a joué un rôle majeur dans l’accélération du flux d'information sur le réseaux sociaux.




-design fluid
-costume celio, anglicisme à tout vas, mon cher réseaux, des requins cyniques  préoccupé par mon 90d
- la mémoire et le temps
- agencement, déplier le paysage, une enquête, commencer par la fin
-

Ensuite je me suis renseignée sur la situation de l'objet d'étude dans le champ de connaissance actuel.
La question sur les temporalités qui résultent de l'apparition des nouveaux médias, bien qu'encore récente, est déja largement traité.
En revanche ce qui est moins investigué c'est plus spécifiquement pour la technologie d’Ajax.
L'enjeux est de comprendre les rouages, souvent pas du tout compris par les designer graphique, montrer en quoi ça a complètement changer nos rapport aux machines ect, comment ça a modifié nos expérience du temps et c’est quoi l'avenir des nouveaux outils web qui utilisent cette technologie .



La technologie Ajax est une avancée technique qui peut sembler anodine ou infime dans l'histoire du web. 
Elle a pourtant radicalement transformé nos interactions avec les interfaces web et a bouleversé nos temporalités devant les écrans.
Avec les bruitages des notifications instantanées , elle est aujourd'hui omniprésente dans nos quotidiens et elle a joué un rôle majeur dans l’accélération du flux d'information sur le réseaux sociaux.


cWho can wait quietly while the mud settles? Who can remain still until the moment of action? Laozi, Tao Te Ching
Pour mieux comprendre comment ces technologies opèrent sur nos expériences du temps dans les interfaces web.

comment c’était avant:
	Aaron swartz dans un de ses articles nous dit que les nouvelles technologies deviennent rapidement si omniprésentes dans nos quotidiens qu'il est parfois difficile de se souvenir 	de comment les choses étaient avant.
Ajax:
Au chant II de l’Iliade, dans le Catalogue des vaisseaux, on lit1 : 
« Les Locriens obéissent au fils d'Oïlée, Ajax le rapide. Il n'a pas la taille du fils de Télamon ; il est moins grand que lui, beaucoup moins grand même. Mais en dépit de sa petite taille et de sa cuirasse de lin, pour lancer la javeline, il n'a pas de rival parmi les Panhellènes ou les Achéens. » 

Ajax pour (Asynchronous javascript and XML) est à permis en 
Même si cette évolution peut sembler anodine ou infime dans l'histoire du web, elle est aujourd'hui omniprésente dans nos quotidiens et je pense qu'il est intérréssant pour le designer d'interface, de web, et théoriciens de la nouvelle communication de comprendre le fonctionnement de ces technologies, leurs histoire et comment elles ont pu sans crier gard modifier nos preception du temps et temporalité devant nos écrans.


En règle générale on remarque que nos appareils de médiation tendent à rester invisibles ou transparents tant qu’ils fonctionnent selon ce que nous attendons d’eux.
En éléminant les frictions sur les sites web du au temps de rafraichisement des pages à chaque nouvelle request au serveur, la technologie Ajax (Asynchronous javascript and XML) à grandement participé à créer ce rythme arrassant de boucles retroactives.
Pour mon mémoire je souhaite m'intérrésser à cette technologie qui a révolutionné le web tel que nous le connaissons aujourd'hui et continue de modeler l'avenir de nos navigation. Elle est aussi en ce moment au coeur de l'actualité du dévellopement web car c'est sur elle que sont basés les frameworks frontend javascript de plus en plus largement utilisés.




It would accept almost anything I typedbut interpret it in a way that was completely different from what I meant. Thishad a lot to do with the fact that I did not have a clue what I was doing, ofcourse, but there is a real issue here: JavaScript is ridiculously liberal in whatit allows. The idea behind this design was that it would make programming inJavaScript easier for beginners. In actuality, it mostly makes finding problemsin your programs harder because the system will not point them out to you.This flexibility also has its advantages, though. It leaves space for a lot oftechniques that are impossible in more rigid languages, and as you will see(for example inChapter 10), it can be used to overcome some of JavaScript’sshortcomings. 



PARTIE 2


